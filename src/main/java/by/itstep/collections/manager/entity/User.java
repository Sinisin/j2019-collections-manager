package by.itstep.collections.manager.entity;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
public class User {

    private Long id;

    private String lastName;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Collection> collections;

    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Comment> comments;

    private Role role;

}
